# Hit Counter Mod

Este mod implementa un contador de golpes recibidos en la esquina superior izquierda del hud, justo entre el hueco de la vida y el borde superior de la pantalla.

El contador se guarda por cada slot de partida y se puede resetear con una tecla (Se puede configurar en las opciones del mod).

Si se ejecuta la secuencia: Stick derecho arriba, Stick derecho abajo 3 veces seguidas (En un intervalo de 3 segundos), se activará el mod ignorar el siguiente golpe (Para poder activar la furia de los caidos de manera legal). En cualquier caso, una vez transcurran 5 segundos se reiniciará  el modo y se volverán a contar los golpes.

Si se pulsa el botón de ignorar hit (Para teclados) el comportamiento será el mismo que antes. Dicho botón se puede configurar en el menú de opciones del mod.

Cualquier duda y/o sugerencia podeis escribirme por twitter a @VikkeDD_

Espero que sirva de ayuda este pequeño experimento que he hecho en un par de tardes para prácticar C# :)

## Sonidos aleatorios al ser golpeado

En el repo encontrareis una carpeta llamada *sounds* que contiene algunos sonidos de prueba. No es necesario descargarla si no quereis. El mod reproducirá un sonido aleatorio que se encuentre en la carpeta *sounds*, en formato .mp3, cada vez que se reciba un golpe.

En el menú del mod se puede desactivar la opción y la carpeta se puede dejar vacía sin ningún problema.

Si añades algún archivo .mp3 al mod acuerdate de reiniciar el juego para que se añada a la lista de sonidos disponibles.

## Como instalar

Tan sencillo como descomprimir el zip en la ruta *<tu_equipo>\steamapps\common\Hollow Knight\hollow_knight_Data\Managed\Mods*

Importante, que la carpeta HitCounter que está dentro del zip quede en la siguiente ruta *steamapps\common\Hollow Knight\hollow_knight_Data\Managed\Mods\HitCounter*, no poner los archivos sueltos

**IMPORTANTE** Son obligatorios los mods: Satchel y MagicUI para que el mod funcione. Ambos se pueden instalar desde Scarab o desde sus respectivos repositorios: https://github.com/BadMagic100/HollowKnight.MagicUI/ | https://github.com/PrashantMohta/Satchel/

**IMPORTANTE 2** Por favor, revisa las opciones del mod en el menú de opciones.
